package guru.springframework.testing.Repository;

import guru.springframework.testing.model.Publisher;
import org.springframework.data.repository.CrudRepository;

public interface PublisherRepository extends CrudRepository<Publisher, Long> {

}
