package guru.springframework.testing.Repository;

import guru.springframework.testing.model.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long> {

}
