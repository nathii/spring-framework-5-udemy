package guru.springframework.testing.Repository;

import guru.springframework.testing.model.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {

}
