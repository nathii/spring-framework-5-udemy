package guru.springframework.testing.bootstrap;

import guru.springframework.testing.Repository.AuthorRepository;
import guru.springframework.testing.Repository.BookRepository;
import guru.springframework.testing.Repository.PublisherRepository;
import guru.springframework.testing.model.Author;
import guru.springframework.testing.model.Book;
import guru.springframework.testing.model.Publisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * At Runtime, this "DevBootstrap" class will get created and managed by the Springframework.
 * Also, AuthorRepository and BookRepository (which we get an implementation of from Spring Data JPA)
 * will be autowired into DevBootstrap
 *
 */
@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;

    /************* Constructor auto injection *******************/
    public DevBootstrap(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent refreshedEvent) {
        initData();
    }

    private void initData(){

        /********************************** initialize **********************************/
        /**
         * Useful when we need to populate our DB with representative dummy data during dev.
         * Entries will only exist in the DB during the execution of the program, and will be
         * destroyed from DB and re=-created on application startup
         *
         */
        Author author = new Author("Nkosinathi", "Mothoa");
        Book book = new Book("The walk of Louie", "AMVKC 3232155 BMNNC", new Publisher("Anchor Inc Publishers", "N/A"));
        author.getBooks().add(book);
        book.getAuthors().add(author);

        authorRepository.save(author);
        bookRepository.save(book);

        Author author2 = new Author("Rego", "Talita");
        Book book2 = new Book("House is a home", "PDKSLM 525141 OACA", new Publisher("Saints Publishers", "N/A"));
        author2.getBooks().add(book2);
        book2.getAuthors().add(author2);

        authorRepository.save(author2);
        bookRepository.save(book2);

        Publisher pub1 = new Publisher("ABH Publishers", "Tailet Reef, LDN");
        Publisher pub2 = new Publisher("Queen Publishers", "Crestville, UK");
        publisherRepository.save(pub1);
        publisherRepository.save(pub2);
    }
}
